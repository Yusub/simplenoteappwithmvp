package com.example.kananyusub.noteappwithmvp.add_note;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.kananyusub.noteappwithmvp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddNoteActivity extends AppCompatActivity
        implements AddNoteContractor.View {

    private AddNoteContractor.Presenter mPresenter;

    @BindView(R.id.edit_text_note_title)
    EditText mEditTextNoteTitle;
    @BindView(R.id.edit_text_note_desc)
    EditText mEditTextNoteDesc;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    private boolean mEnableTouchEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        new AddNotePresenter(this);
        ButterKnife.bind(this);

        mEnableTouchEvents = true;
    }

    @OnClick(R.id.button_add)
    void callPresenterAdder() {
        mPresenter.onSaveClickListener();
    }

    @OnClick(R.id.button_cancel)
    void cancelAddingProcess() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void setPresenter(AddNotePresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public String getNoteTitle() {
        return mEditTextNoteTitle.getText().toString().trim();
    }

    @Override
    public String getNoteDescription() {
        return mEditTextNoteDesc.getText().toString().trim();
    }

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoteTitleValidationError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoteDescriptionValidationError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void freezeScreen() {
        mEnableTouchEvents = false;
    }

    @Override
    public void disableFreezeScreen() {
        mEnableTouchEvents = true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return mEnableTouchEvents && super.dispatchTouchEvent(ev);
    }
}
