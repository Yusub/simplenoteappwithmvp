package com.example.kananyusub.noteappwithmvp.main;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kananyusub.noteappwithmvp.R;
import com.example.kananyusub.noteappwithmvp.interactors.NoteInteractor;

import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<NoteInteractor> mNoteInteractorList;

    public NoteAdapter(Context context, List<NoteInteractor> noteInteractorList) {
        mContext = context;
        mNoteInteractorList = noteInteractorList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_note, viewGroup, false);
        return new NoteHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        NoteHolder holder = (NoteHolder) viewHolder;
        holder.bind(mNoteInteractorList.get(i));
    }

    @Override
    public int getItemCount() {
        return mNoteInteractorList != null ? mNoteInteractorList.size() : 0;
    }
}
