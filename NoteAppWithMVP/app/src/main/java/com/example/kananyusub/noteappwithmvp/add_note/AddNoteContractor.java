package com.example.kananyusub.noteappwithmvp.add_note;

import android.content.Context;

import com.example.kananyusub.noteappwithmvp.BaseView;

public interface AddNoteContractor {

    interface View extends BaseView<AddNotePresenter> {

        String getNoteTitle();

        String getNoteDescription();

        void showProgressBar();

        void hideProgressBar();

        void onSuccess(String message);

        void onFailure(String message);

        void onNoteTitleValidationError(String message);

        void onNoteDescriptionValidationError(String message);

        Context getContext();

        void freezeScreen();

        void disableFreezeScreen();
    }


    interface Presenter {

        void onSaveClickListener();


    }

}
