package com.example.kananyusub.noteappwithmvp.add_note;

public interface AddNoteCallBack {

    void onSuccess(String message);

    void onFailure(String message);

}
