package com.example.kananyusub.noteappwithmvp.add_note;

import android.os.Handler;

import com.example.kananyusub.noteappwithmvp.interactors.NoteInteractor;

public class AddNotePresenter implements AddNoteContractor.Presenter {

    private AddNoteContractor.View mView;
    private NoteInteractor mInteractor;

    public AddNotePresenter(AddNoteContractor.View view) {
        mView = view;
        mView.setPresenter(this);
        mInteractor = new NoteInteractor(mView.getContext());
    }

    @Override
    public void onSaveClickListener() {

        if (mView != null) {

            mView.showProgressBar();
            mView.freezeScreen();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    mView.hideProgressBar();
                    mView.disableFreezeScreen();

                    if (mInteractor != null) {


                        if (mView.getNoteTitle() == null ||
                                mView.getNoteTitle().isEmpty()) {
                            mView.onNoteTitleValidationError("title nulldur ve ya bosdur!");
                            return;
                        }

                        if (mView.getNoteDescription() == null ||
                                mView.getNoteDescription().isEmpty()) {
                            mView.onNoteDescriptionValidationError("desc nulldur ve ya bosdur!");
                            return;
                        }


                        mInteractor.addNote(mView.getNoteTitle(),
                                mView.getNoteDescription(),
                                new AddNoteCallBack() {
                                    @Override
                                    public void onSuccess(String message) {
                                        mView.onSuccess(message);
                                    }

                                    @Override
                                    public void onFailure(String message) {
                                        mView.onFailure(message);
                                    }
                                });


                    } else {
                        mView.onFailure("Interactor nulldur!");
                    }

                }
            }, 1500);

        }

    }
}
