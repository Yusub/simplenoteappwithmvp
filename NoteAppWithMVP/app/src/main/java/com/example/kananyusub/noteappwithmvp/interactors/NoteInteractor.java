package com.example.kananyusub.noteappwithmvp.interactors;

import android.content.Context;

import com.example.kananyusub.noteappwithmvp.add_note.AddNoteCallBack;
import com.example.kananyusub.noteappwithmvp.database.NoteDatabase;

import java.util.List;

public class NoteInteractor {

    private int mNoteId;
    private String mNoteTitle;
    private String mNoteDescription;
    private Context mContext;
    private NoteDatabase noteDatabase;


    public NoteInteractor(Context context) {
        mContext = context;
        noteDatabase = new NoteDatabase(mContext);
    }

    public NoteInteractor() {

    }

    public int getNoteId() {
        return mNoteId;
    }

    public void setNoteTitle(String noteTitle) {
        mNoteTitle = noteTitle;
    }

    public String getNoteTitle() {
        return mNoteTitle;
    }

    public void setNoteDescription(String noteDescription) {
        mNoteDescription = noteDescription;
    }

    public String getNoteDescription() {
        return mNoteDescription;
    }


    public List<NoteInteractor> getNoteListFromDb() {
        return noteDatabase.getAllNOtes();
    }

    public void addNote(String title, String desc, AddNoteCallBack callBack) {

        NoteInteractor noteInteractor = new NoteInteractor();
        noteInteractor.setNoteTitle(title);
        noteInteractor.setNoteDescription(desc);

        boolean result = noteDatabase.addNote(noteInteractor);

        if (result) {
            callBack.onSuccess("database-e ugurla yazildi :)");
        } else {
            callBack.onFailure("database-e yazilarken, problem bas verdi!");
        }

    }
}
