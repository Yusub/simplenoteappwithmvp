package com.example.kananyusub.noteappwithmvp;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
