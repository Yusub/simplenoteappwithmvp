package com.example.kananyusub.noteappwithmvp.main;

import android.os.Handler;

import com.example.kananyusub.noteappwithmvp.interactors.NoteInteractor;

import java.util.List;

public class MainPresenter implements MainContractor.Presenter {

    private MainContractor.View mView;
    private NoteInteractor mNoteInteractor;

    public MainPresenter(MainContractor.View view) {
        mView = view;
        mView.setPresenter(this);
        mNoteInteractor = new NoteInteractor(mView.getContext());
    }


    @Override
    public void getNoteList() {

        if (mView != null) {

            mView.showProgressBar();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {


                    if (mNoteInteractor != null) {
                        List<NoteInteractor> noteInteractorList = mNoteInteractor.getNoteListFromDb();


                        if (noteInteractorList != null) {

                            if (noteInteractorList.size() > 0) {
                                mView.setNoteList(noteInteractorList);
                            } else {
                                mView.onError("list bosdur!");
                            }

                        } else {
                            mView.onError("noteInteractorList nulldur!");
                        }


                    } else {

                        mView.onError("note interactor nulldur!");

                    }

                    mView.hideProgressBar();
                }

            }, 1500);



        }


    }
}
