package com.example.kananyusub.noteappwithmvp.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.kananyusub.noteappwithmvp.R;
import com.example.kananyusub.noteappwithmvp.interactors.NoteInteractor;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoteHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_note_title)
    TextView mTextViewNoteTitle;
    @BindView(R.id.text_note_description)
    TextView mTextViewNoteDescription;


    public NoteHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }


    public void bind(NoteInteractor noteInteractor) {
        mTextViewNoteTitle.setText(noteInteractor.getNoteTitle());
        mTextViewNoteDescription.setText(noteInteractor.getNoteDescription());
    }
}
