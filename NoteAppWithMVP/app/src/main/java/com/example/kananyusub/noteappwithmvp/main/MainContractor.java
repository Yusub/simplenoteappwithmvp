package com.example.kananyusub.noteappwithmvp.main;

import android.content.Context;

import com.example.kananyusub.noteappwithmvp.BaseView;
import com.example.kananyusub.noteappwithmvp.interactors.NoteInteractor;

import java.util.List;

public interface MainContractor {

    interface View extends BaseView<MainPresenter> {

        Context getContext();

        void showProgressBar();

        void hideProgressBar();

        void setNoteList(List<NoteInteractor> noteList);

        void onError(String message);

    }


    interface Presenter {

        void getNoteList();

    }

}
