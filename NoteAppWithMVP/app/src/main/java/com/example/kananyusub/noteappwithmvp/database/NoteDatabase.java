package com.example.kananyusub.noteappwithmvp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.kananyusub.noteappwithmvp.interactors.NoteInteractor;

import java.util.ArrayList;
import java.util.List;

public class NoteDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "note.db";
    private static final String TABLE_NAME = "customers";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NOTE_TITLE = "note_title";
    private static final String COLUMN_NOTE_DESCRPTION = "note_description";


    public NoteDatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + "(" +
                COLUMN_ID + " integer primary key autoincrement, " +
                COLUMN_NOTE_TITLE + " text, " +
                COLUMN_NOTE_DESCRPTION + " text " +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(db);
    }

    public boolean addNote(NoteInteractor noteInteractor) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_NOTE_TITLE, noteInteractor.getNoteTitle());
        contentValues.put(COLUMN_NOTE_DESCRPTION, noteInteractor.getNoteDescription());

        long result = db.insert(TABLE_NAME, null, contentValues);
        db.close();

        if (result != -1) {
            return true;
        } else {
            return false;
        }
    }

    public List<NoteInteractor> getAllNOtes() {
        String query = "select * from " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        NoteInteractor currentNote;
        List<NoteInteractor> notes = new ArrayList<>();

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast() == true) {
                currentNote = new NoteInteractor();
                currentNote.setNoteTitle(cursor.getString(cursor.getColumnIndex(COLUMN_NOTE_TITLE)));
                currentNote.setNoteDescription(cursor.getString(cursor.getColumnIndex(COLUMN_NOTE_DESCRPTION)));

                notes.add(currentNote);
                cursor.moveToNext();
            }
        }

        cursor.close();
        db.close();

        return notes;
    }
}
